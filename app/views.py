from django.shortcuts import render
from .forms import InputForm
from subprocess import call

import subprocess

# Create your views here.
def index(request):
    context = {
        'form': InputForm,
    }
    param1 = request.GET.get('option', '')
    if param1 != '':
        handle_request(request)
        return render(request, 'index.html', context)
    else:
        return render(request, 'index.html', context)

def handle_request(request):
    option = request.GET.get('option')
    if option == '1':
        command = '/home/broprog//Documents/Test with App/broprog-master/app/midtier2.sh'
        subprocess.check_call([command, "1"])
    elif option == '2':
        rate = request.GET.get('rate')
        output = request.GET.get('output')
        command = '/home/broprog//Documents/Test with App/broprog-master/app/midtier2.sh'
        subprocess.check_call([command, "2", str(rate), str(output)])
    elif option == '3':
        filename = request.GET.get('filename')
        command = '/home/broprog//Documents/Test with App/broprog-master/app/midtier2.sh'
        subprocess.check_call([command, "3", str(filename)])
    else:
        print('The input is invalid!')
