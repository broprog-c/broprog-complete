from django import forms

class InputForm(forms.Form):
    option = forms.CharField(max_length = 5,
        widget = forms.TextInput(
            attrs = {
                'class':'form-control', 'id':'option', 'placeholder':'Pilih option 1-4', 'name':'option', 'required':'required',
            }
        ),
    required=True)

    rate = forms.CharField(max_length = 5,
        widget = forms.TextInput(
            attrs = {
                'class':'form-control', 'id':'rate', 'placeholder':'Masukkan rate', 'name':'rate', 
            }
        ),
    required=False)

    output = forms.CharField(max_length = 50,
        widget = forms.TextInput(
            attrs = {
                'class':'form-control', 'id':'output', 'placeholder':'Masukkan nama file output', 'name':'output', 
            }
        ),
    required=False)

    filename = forms.CharField(max_length = 50,
        widget = forms.TextInput(
            attrs = {
                'class':'form-control', 'id':'filename', 'placeholder':'Masukkan filename image', 'name':'filename', 
            }
        ),
    required=False)
