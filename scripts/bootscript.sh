#!/bin/bash

### BEGIN INIT INFO
#Provides: install
#Required-Start: $all
#Required-Stop:
#Default-Start: 2 3 4 5
#Default-Stop:
#Short-Description: install custom device driver
### END INIT INFO

echo "Removing Unused Service"
cat unused_service | while read line; do systemctl disable $line; done
echo "Finished Removing Unused Service"

echo "Install Driver"
chmod +x main_script.sh
cp main_script.sh ~/.main_script.sh
chmod +x ~/.main_script.sh
sudo echo "bash ~/.main_script.sh" >> ~/.bashrc
echo "Driver Successfully Installed"
source ~/.bashrc
