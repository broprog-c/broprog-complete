#!/bin/bash

main_menu() {
    read -p "
    ========================================
                   MAIN MENU
    ========================================
    1. Setup & Install Dependencies
    2. Modinfo
    3. Modify exclusive_caps
    4. Exit
    Choose 1-4: " menu_option

    if [ $menu_option == 1 ]; then
        setup
    elif [ $menu_option == 2 ]; then
        modinfo_func
    elif [ $menu_option == 3 ]; then
        modify_arg
    elif [ $menu_option == 4 ]; then
        echo "Bye Bye...."
        echo "Press [Enter] key to continue..."
        while [ true ] ; do
            read -s -N 1 -t 1 key
            if [[ $key == $'\x0a' ]] ; then
                exit 1
            fi
        done
    else
        echo "Invalid input, try again!"
        main_menu
    fi
}

setup() {
    echo "-------------------------------------------------------"
    echo "            Setup and Install Dependencies             "
    echo "-------------------------------------------------------"
    
    echo "---- Initiate Setup for v4l2loopback"
    sudo apt-get update
    sudo apt-get remove v4l2loopback-dkms
    sudo apt-get install make build-essential libelf-dev linux-headers-$(uname -r) unzip
    wget https://github.com/umlaeute/v4l2loopback/archive/master.zip
    unzip master.zip
    rm -rf master.zip

    echo "---- Installing Dependencies"
    sudo apt update
    sudo apt install v4l-utils ffmpeg

    echo "Press [Enter] key to continue..."
    while [ true ] ; do
        read -s -N 1 -t 1 key
        if [[ $key == $'\x0a' ]] ; then
            main_menu
        fi
    done
}

modinfo_func() {
    echo "-------------------------------------------------------"
    echo "                        Modinfo                        "
    echo "-------------------------------------------------------"
        
    modinfo v4l2loopback

    echo "Press [Enter] key to continue..."
    while [ true ] ; do
        read -s -N 1 -t 1 key
        if [[ $key == $'\x0a' ]] ; then
            main_menu
        fi
    done
}

modify_arg() {
    echo "-------------------------------------------------------"
    echo "                Change exclusive_caps                  "
    echo "-------------------------------------------------------"
    
    modprobe v4l2loopback exclusive_caps=1

    success_checker=`echo $?`

    if [ $success_checker -eq "0" ]; then
        echo "V4L2LOOPBACK --- Argumen exlusive_caps successfully enabled!"
    else
        echo "V4L2LOOPBACK --- Argumen exlusive_caps failed to enable :("
    fi
}

main_menu